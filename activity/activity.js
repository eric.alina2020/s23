//TASK No. 1: Insert a single room using the insertOne() method.

db.rooms.insertOne(
    {
        name: "single",
        accommodates: 2,
        price: 1000,
        description: "A simple room with all the basic necessities",
        rooms_available: 10,
        isAvailable: false
    }
);

//JSON format
/*
    {
        "_id" : ObjectId("6241868405c9c76f2a58ca71"),
        "name" : "single",
        "accommodates" : 2.0,
        "price" : 1000.0,
        "description" : "A simple room with all the basic necessities",
        "rooms_available" : 10.0,
        "isAvailable" : false
    }
*/


//TASK No. 2: Insert multiple rooms using the insertMany() method.

db.rooms.insertMany([
    {
        name: "double",
        accommodates: 3,
        price: 2000,
        description: "A room fit for a small family going on a vacation",
        rooms_available: 5,
        isAvailable: false
    },
    {
        name: "queen",
        accommodates: 4,
        price: 4000,
        description: "A room with a queen-sized bed perfect for a simple getaway",
        rooms_available: 15,
        isAvailable: false
    }
]);

//JSON format
/*
    {
        "_id" : ObjectId("6241887f05c9c76f2a58ca72"),
        "name" : "double",
        "accommodates" : 3.0,
        "price" : 2000.0,
        "description" : "A room fit for a small family going on a vacation",
        "rooms_available" : 5.0,
        "isAvailable" : false
    }

    {
        "_id" : ObjectId("6241887f05c9c76f2a58ca73"),
        "name" : "queen",
        "accommodates" : 4.0,
        "price" : 4000.0,
        "description" : "A room with a queen-sized bed perfect for a simple getaway",
        "rooms_available" : 15.0,
        "isAvailable" : false
    }
*/


//TASK No. 3: Search for a room with the name 'double' using the find() method.

db.rooms.find({name: "double"})


//TASK N0. 4: Update the queen room and set the available rooms to 0 using the updateOne() method.
db.rooms.updateOne({name: "queen"}, {$set: {rooms_available: 0}});

//TASK NO. 5: Delete all rooms that have 0 availability using the deleteMany() method.
db.rooms.deleteMany({rooms_available: 0});