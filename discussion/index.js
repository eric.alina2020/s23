// CRUD OPERATIONS

/*
	(CREATE) Inserting Documents
	Syntaxes:
		db.collectionName.insertOne({object});
		db.collectionName.insert({object})

		object.object.method({object})

	The mongo shell also uses js for syntax which makes it convenient for us to understand its code. Creating mongoDB syntax in a text editor makes it easy for us to modify code.
*/


	//EXAMPLE:
	db.users.insert({
	    firstName: "Jane",
	    lastName: "Doe",
	    age: 21,
	    contact: {
	        phone: "87654321",
	        email: "janedoe@gmail.com"
	        },
	    courses: ["CSS", "Javascript", "Python"],
	    department: "none"
	});


/*	
	Insert Many -> inserts multiple documents
	Syntax:
		db.collectionName.insertMany([{objectA}, {objectB}])
*/

	//EXAMPLE:
	db.users.insertMany([
	{
	    firstName: "Stephen",
	    lastName: "Hawking",
	    age: 76,
	    contact: {
	        phone: "87654321",
	        email: "stephenhawking@gmail.com"
	        },
	    courses: ["Python", "React", "PHP"],
	    department: "none"
	},
	{
	    firstName: "Neil",
	    lastName: "Armstrong",
	    age: 82,
	    contact: {
	        phone: "87654321",
	        email: "neilarmstrong@gmail.com"
	        },
	    courses: ["React", "RLaravel", "Sass"],
	    department: "none"
	}
]);


/*
	(READ) Finding Documents
	Syntax:
		db.collectionName.find({});
		db.collectionName.find({field: value});
*/

	//EXAMPLE:

	// Finding a single document
	db.users.find({ firstName: "Stephen" });
	// If the documents match the criteria, the document will return the match.

	//Finding with multiple parameters
	db.users.find({
		lastName: "Armstrong",
		age: 82
	});

	db.users.find({});
	//Leaving the search criteria empty will retrieve ALL the documents


/*
	(UPDATE) Updating Documents
	Syntax:
		db.collectionName.updateOne({criteria}, {$set: {field: value}})
*/

	// For Update Demo:
	// Creating a document to update
		db.users.insert({
			firstName: "Test",
		    lastName: "Test",
		    age: 0,
		    contact: {
			        phone: "00000000",
			        email: "test@gmail.com"
		    	},
		    courses: [],
		    department: "none"
		});

	//EXAMPLE:
	db.users.updateOne(
		{firstName: "Test"}, 
		{$set: 
			{
			    firstName: "Bill",
			    lastName: "Gates",
			    age: 65,
			    contact: {
				        phone: "12345678",
				        email: "billgates@gmail.com"
			        },
			    courses: ["PHP", "Laravel", "HTML"],
			    department: "Operations",
			    status: "active"
	    	}
	    }
    );

/*
	Update Many -> updates multiple documents
	Syntax:
		db.collectionName.updateMany({criteria}, {$set: {field: value}})
*/

	//EXAMPLE:
	db.users.updateMany(
		{department: "none"}, 
		{ 
			$set: {department: "HR"}
		}
	);


/*	
	REPLACE ONE -> used if replacing the whole document is necessary.
	Syntax:
		db.collectionName.replaceOne({criteria}, {fields need to change})
*/

	//EXAMPLE:
	db.users.replaceOne(
		{firstName: "Bill"}, 
		{
		    firstName: "Bill",
		    lastName: "Gates",
		    age: 65,
		    contact: {
			        phone: "12345678",
			        email: "bill@gmail.com"
		        },
		    courses: ["PHP", "Laravel", "HTML"],
		    department: "Operations" 
	    }
	);	


/*
	(DELETE) Deleting document
	Syntax:
		db.collectionName.deleteOne({criteria})
*/

	// For delete demo:
	//Create a document to delete
		db.users.insert({
		    firstName: "test",
		    lastName: "test"
		});

	//EXAMPLE:
	//Deleting a single document
	db.users.deleteOne({firstName: "test"});


/*
	Delete MAny -> deletes multiple documents
	Syntax:
		db.collectionName.deleteMany({criteria})
		
	NOTE: Be careful when using the "deleteMany" method. If no search criteria is provided, it will delete all documents in a database.
*/
	//EXAMPLE:
	db.users.deleteMany({});


/*
	ADVANCED QUERIES
	-> Retrieving data with complex data structures is also a good skill for any developer to have.
	-> Real world examples of data can be as complex as having two or more layers of nested objects and arrays.
	-> Learning to query these kinds of data is also essential to ensure that we are able to retrieve any information that we would need in our application.
*/

	//EXAMPLES:

	//Query an embedded document:
	db.users.find({
		contact: {
			phone: "87654321",
			email: "stephenhawking@gmail.com"
		}
	});

	//Query on a nested field:
	db.users.find({
		"contact.email": "janedoe@gmail.com"
	});

	//Query an array with exact elements:
	db.users.find({ 
		courses: [ "CSS", "Javascript", "Python"]
	});

	// Query an array without any regard to order:
	db.users.find({ 
		courses: {$all: ["React", "Python"]} 
	});

	//Query an embedded array of objects:
	//insert data for demo:
	db.users.insert({
    	nameArray: [{
        	nameA: "Juan"
    	},
    	{
        	nameB: "Tamad"
    	}]
    });

    //then,
    db.users.find({
    	nameArray: {nameA: "Juan"}
	});

/*

ACTIVITY:
1. Create an activity.js file on where to write and save the solution for the activity.

2. Create a database of hotel with a collection of rooms.

3. Insert a single room (insertOne method) with the following details:

- name - single
- accomodates - 2
- price - 1000
- description - A simple room with all the basic necessities
- rooms_available - 10
- isAvailable - false

4. Insert multiple rooms (insertMany method) with the following details:

- name - double
- accomodates - 3
- price - 2000
- description - A room fit for a small family going on a vacation
- rooms_available - 5
- isAvailable - false

- name - queen
- accomodates - 4
- price - 4000
- description - A room with a queen sized bed perfect for a simple getaway
- rooms_available - 15
- isAvailable - false

5. Use the find method to search for a room with the name double.

6. Use the updateOne method to update the queen room and set the available rooms to 0.

7. Use the deleteMany method rooms to delete all rooms that have 0 availability.

8. Create a git repository named S23.

9. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code."s23 Activity"

10. Add the link in Boodle. "MongoDB - CRUD Operations"

*/